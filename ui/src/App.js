import 'bulma/bulma.sass'
import 'primereact/resources/themes/nova-light/theme.css'
import 'primereact/resources/primereact.min.css'
import 'primeicons/primeicons.css'

import React, { Component } from 'react'
import { Route, Link} from 'react-router-dom'

import Login from './page/login/'
import Customer from './page/customer/'
import Contact from './page/contact/'
import Project from './page/project/'
import Add_Edit from './page/project/Add_Edit'
import Management from './page/manage'
import Logout from './page/logout'
document.addEventListener('DOMContentLoaded', () => {
  // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0)
  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {
    // Add a click event on each of them
    $navbarBurgers.forEach( el => {
      el.addEventListener('click', () => {
        // Get the target from the "data-target" attribute
        const target = el.dataset.target
        const $target = document.getElementById(target)
        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active')
        $target.classList.toggle('is-active')
      })
    })
  }
})

class App extends Component {
  constructor() {
    super();
    this.state={
      permission:false
    }
  }
  componentDidMount() {
      this.setState({permission: localStorage.getItem('Permission_id')})

  }
    render() {

      if(this.state.permission == 2 || this.state.permission == 3 || this.state.permission == 4 || this.state.permission == 6) { // ถ้าเป็น user ปกติ
        return (
          <div>
            <nav className="navbar is-info" role="navigation" aria-label="main navigation">
              <div className="navbar-brand">
                  <Link to='/' className="navbar-item">
                  <img src="./logoPNG.png" width="100px" hight="290px" />
                  </Link>
                <a role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                  <span aria-hidden="true"></span>
                  <span aria-hidden="true"></span>
                  <span aria-hidden="true"></span>
                </a>
              </div>

              <div id="navbarBasicExample" className="navbar-menu">
                <div className="navbar-start">
                <Link to='/Customer' className="navbar-item">Customer </Link>
                  <div className="navbar-item has-dropdown is-hoverable">
                    <a className="navbar-link">
                      More
                    </a>
                    <div className="navbar-dropdown">
                      <a className="navbar-item" href="http://nccs.co.th">
                        About
                      </a>
                      <a className="navbar-item" href="https://facebook.com/patipan.winthachai">
                        Developer
                      </a>
                    </div>
                  </div>
                </div>

                <div className="navbar-end">
                  <div className="navbar-item">
                    <div className="buttons">
                      <Link className="button is-light" to='/Logout'>
                        Logout
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </nav>
            <Route exact path="/" component={Customer}/>
            <Route exact path="/Customer" component={Customer} />
            <Route exact path="/Contact/:id" component={Contact} />
            <Route exact path="/Project/:id" component={Project} />
            <Route exact path="/Project/:id1/:id2" component={Add_Edit} />
            <Route exact path="/Logout" component={Logout} />
          </div>


        )


      } else if(this.state.permission == 1) { // ถ้าเป็น Admin

        return (
          <div>
            <nav className="navbar is-info" role="navigation" aria-label="main navigation">
              <div className="navbar-brand">
                  <Link to='/' className="navbar-item">
                  <img src="./logoPNG.png" width="100px" hight="290px" />
                  </Link>
                <a role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                  <span aria-hidden="true"></span>
                  <span aria-hidden="true"></span>
                  <span aria-hidden="true"></span>
                </a>
              </div>

              <div id="navbarBasicExample" className="navbar-menu">
                <div className="navbar-start">
                <Link to='/Customer' className="navbar-item">Customer </Link>
                <Link to='/Management' className="navbar-item">Management </Link>

                  <div className="navbar-item has-dropdown is-hoverable">
                    <a className="navbar-link">
                      More
                    </a>
                    <div className="navbar-dropdown">
                      <a className="navbar-item" href="http://nccs.co.th">
                        About
                      </a>
                      <a className="navbar-item" href="https://facebook.com/patipan.winthachai">
                        Developer
                      </a>
                    </div>
                  </div>
                </div>

                <div className="navbar-end">
                  <div className="navbar-item">
                    <div className="buttons">
                      <Link className="button is-light" to='/Logout'>
                        Logout
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </nav>

            <Route exact path="/" component={Customer}/>
            <Route exact path="/Customer" component={Customer} />
            <Route exact path="/Contact/:id" component={Contact} />
            <Route exact path="/Project/:id" component={Project} />
            <Route exact path="/Project/:id1/:id2" component={Add_Edit} />
            <Route exact path="/Management" component={Management} />
            <Route exact path="/Logout" component={Logout} />

          </div>


        )

      }else {

        return (
          <div>
            <nav className="navbar is-info" role="navigation" aria-label="main navigation">
              <div className="navbar-brand">
                  <Link to='/' className="navbar-item">
                  <img src="./logoPNG.png" width="100px" hight="290px" />
                  </Link>
                <a role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                  <span aria-hidden="true"></span>
                  <span aria-hidden="true"></span>
                  <span aria-hidden="true"></span>
                </a>
              </div>

              <div id="navbarBasicExample" className="navbar-menu">
                <div className="navbar-start">
                  <div className="navbar-item has-dropdown is-hoverable">
                    <a className="navbar-link">
                      More
                    </a>
                    <div className="navbar-dropdown">
                      <a className="navbar-item" href="http://nccs.co.th">
                        About
                      </a>
                      <a className="navbar-item" href="https://facebook.com/patipan.winthachai">
                        Developer
                      </a>
                    </div>
                  </div>
                </div>

              </div>
            </nav>
            <Route component={Login} />

          </div>


        )
      }

    }


}

export default App
