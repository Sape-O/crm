const api = require('express').Router()
const session = require('express-session')

api.use(session({
  secret: 'ssshhhhh',
  resave:false,
  saveUninitialized:true
}))

module.exports = api
