const api = require('express').Router()

api.use('/', require('./customer/events'))
api.use('/', require('./contact/events'))
api.use('/', require('./authen/login'))
api.use('/', require('./project/events'))
api.use('/', require('./etc/getTableDB'))
api.use('/', require('./management'))

module.exports = api
