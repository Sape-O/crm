const api = require('express').Router()
const db = require('../../model/connect_DB')

api.use(require('../../model/session')) //session
/*
api.get('/contact/:id', async (req, res) => {
  try {
    res.send("GET : /contact/"+req.params.id)
  } catch(err) {
    res.status(400).send(err)
  }
})
*/

// get contact for list all in contact page
api.get('/contact/log/:id', (req, res) => {

  let id = req.params.id
  try {

    var querySQL = "SELECT *\
                            FROM contact_log \
                            INNER JOIN customers \
                            ON customers.Customer_Id = contact_log.Customer_id\
                            WHERE contact_log.Customer_id='"+id+"'\
                            ORDER  BY Contact_log_Id DESC"

    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      else {
        for(var i=0;i<results.length;i++) {
          results[i].Date_contact = new Date(results[i].Date_contact)
          results[i].Date_contact =  ((results[i].Date_contact.getDate()+1 <10?'0':'') + (results[i].Date_contact.getDate() + 1))
                             + "-" + ((results[i].Date_contact.getMonth()+1 <10?'0':'')+(results[i].Date_contact.getMonth() + 1))
                             + "-" + results[i].Date_contact.getFullYear()
        }
        res.send(results)
      }

    })
  } catch(err) {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }
})

api.post('/contact/newcontact/:id', async (req,res) => {
  try {
    let Customer_Id = req.params.id
    let Date_contact = new Date(req.body.Date_contact).toISOString().slice(0, 19).replace('T', ' ')
    let Method = req.body.Method
    let Notes =  req.body.Notes
    //Date_contact = Date_contact.getDate() + "-" + (Date_contact.getMonth() + 1) + "-" + Date_contact.getFullYear()
    var querySQL = "INSERT INTO contact_log (Customer_Id,\
                                             Date_contact,\
                                             Method,\
                                             Notes)\
                                             VALUES ('"+Customer_Id+"',\
                                                      '"+Date_contact+"',\
                                                       '"+Method+"',\
                                                        '"+Notes+"')"
    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      res.send("Success")
    })
  } catch(err) {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }
})

api.post('/contact/edit/:id', async (req, res) => {
  let Date_contact = new Date(req.body.Date_contact).toISOString().slice(0, 19).replace('T', ' ')
  let Method  = req.body.Method
  let Notes = req.body.Notes
  let Contact_log_Id = req.params.id
  let valueUpdate = [Date_contact,Method,Notes,Contact_log_Id]

  try {

    let querySQL = "UPDATE contact_log\
                    SET Date_contact=?,\
                        Method=?,\
                        Notes=?\
                    WHERE Contact_log_Id=?"
    db.query(querySQL,valueUpdate,(err,results) => {
      if(err) res.send("0")
      else res.send("Edit Success")
    })

  } catch(err) {
    res.status(400).send(err)
  }

})
/*
api.delete('/contact/delete/:id', async (req, res) => { // Dangerus for only supper admin

  try {
    res.send("Delete :/contact/delete/"+req.params.id)
  } catch(err) {
    res.status(400).send(err)
  }

})
*/

module.exports = api
