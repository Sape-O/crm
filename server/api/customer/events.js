const api = require('express').Router()
const db = require('../../model/connect_DB')

api.use(require('../../model/session'))

api.get('/customers', async (req, res) => {

  try {
    var querySQL = "SELECT * FROM customers ORDER  BY Customer_Id DESC"
    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      res.send(results)
    })
  } catch(err) {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }
})

api.post('/customers/newcustomer', async (req, res) => {
  try {
    let Name_company = req.body.Name_company
    let Address_company = req.body.Address_company
    let Name_contact = req.body.Name_contact
    let Position_contact = req.body.Position_contact
    let Email_contact = req.body.Email_contact
    let Tel_desk = req.body.Tel_desk
    let Tel_mobile = req.body.Tel_mobile

    let querySQL = "INSERT INTO customers (\
                                            Name_company,\
                                            Address_company,\
                                            Name_contact,\
                                            Position_contact,\
                                            Email_contact,\
                                            Tel_desk,\
                                            Tel_mobile)\
                                            VALUES (\
                                              '"+Name_company+"',\
                                              '"+Address_company+"',\
                                              '"+Name_contact+"',\
                                              '"+Position_contact+"',\
                                              '"+Email_contact+"',\
                                              '"+Tel_desk+"',\
                                              '"+Tel_mobile+"')"

    db.query(querySQL,(err,results) => {
      if(err) res.send(error)
      else res.send("Success")
    })

  } catch(err) {
    res.status(400).ssend(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }

})

api.post('/customer/edit', async (req, res) => {
  let Customer_Id = req.body.Customer_Id
  let Name_company = req.body.Name_company
  let Address_company = req.body.Address_company
  let Name_contact = req.body.Name_contact
  let Position_contact = req.body.Position_contact
  let Email_contact = req.body.Email_contact
  let Tel_desk = req.body.Tel_desk
  let Tel_mobile = req.body.Tel_mobile
  try {
    let valueUpdate = [Name_company,Address_company,Name_contact,Position_contact,Email_contact,Tel_desk,Tel_mobile,Customer_Id]
    let querySQL = "UPDATE customers\
                    SET Name_company=?,\
                        Address_company=?,\
                        Name_contact=?,\
                        Position_contact=?,\
                        Email_contact=?,\
                        Tel_desk=?,\
                        Tel_mobile=?\
                    WHERE Customer_Id=?"
    db.query(querySQL,valueUpdate,(err,results) => {
      if(err) res.send(error)
      else res.send("Update Success")
    })
  } catch(err) {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }
})


/*
api.delete('/customer/delete/:id', async (req, res) => { // Dangerus not for only supper admin

  try { //edit customer check condition and update to db
    res.send("Delete : /customer/delete/"+req.params.id)
  } catch(err) {
    res.status(400).send(err)
  }

})
*/
module.exports = api
