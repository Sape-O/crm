const api = require('express').Router()
const db = require('../../model/connect_DB')

api.use(require('../../model/session'))// session


// ใช้ในหน้า Management แสดง user
api.get('/user', async (req, res) => {
  try {
    let querySQL = "SELECT * FROM user INNER JOIN permission ON permission.Permission_Id = user.Permission_id"
    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      else
        res.send(results)
    })
  } catch {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }
})

// for edit user edit
api.get('/user/:id', async (req, res) => {
  console.log(req.params.id)
  // password จะหลุดไปด้วย อย่าใช้ * ในการ query
  try {
    let querySQL = "SELECT * FROM user WHERE User_Id = '"+req.params.id+"' "
    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      else
        res.send(results)
    })
  } catch {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }
})



api.get('/type_technology/', async (req, res) => {
  try {
    let querySQL = "SELECT * FROM type_technology"
    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      res.send(results)
    })
  } catch {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }
})

api.get('/vendor', async (req, res) => {
  try {
    let querySQL = "SELECT * FROM vendor"
    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      res.send(results)
    })
  } catch {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }
})

api.get('/vendor/:id', async (req, res) => {
  let Vendor_Id = req.params.id
  try {
    let querySQL = "SELECT * FROM vendor WHERE Vendor_Id='"+Vendor_Id+"' "
    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      res.send(results)
    })
  } catch {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }
})

// ใช้ในการแสดงค่า kind ตอน edit project
api.get('/kind_type_tech/:id', async (req, res) => {
	let Id = req.params.id
	try {
		let querySQL = "SELECT * FROM kind_type_tech WHERE kind_type_tech.Type_technology_id = '"+Id+"' "
    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      res.send(results)
    })
	} catch {
		res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
	}
})
//
api.get('/kind_type_tech/', async (req, res) => {
	let Id = req.params.id
	try {
		let querySQL = "SELECT * FROM kind_type_tech \
                    INNER JOIN type_technology \
                    ON kind_type_tech.Type_technology_id=type_technology.Type_technology_Id \
                    ORDER BY kind_type_tech.Type_technology_id "
    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      res.send(results)
    })
	} catch {
		res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
	}
})

// page management เอาไว้ select
api.get('/permission', async (req, res) => {
  try {
    let querySQL = "SELECT * FROM permission"
    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      else
        res.send(results)
    })
  } catch {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }
})


api.get('/type_technology/', async (req, res) => {
	let Id = req.params.id
	try {
		let querySQL = "SELECT * FROM type_technology "
    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      res.send(results)
    })
	} catch {
		res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
	}
})

module.exports = api
