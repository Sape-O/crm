const api = require('express').Router()
const db = require('../../model/connect_DB')

api.use(require('../../model/session'))// session

api.post('/management/newKind/', async (req,res) => {
	let typeIdSelect = req.body.typeIdSelect
	let Name_kind_type_tech = req.body.Name_kind_type_tech
	try {

    var querySQL = "INSERT INTO kind_type_tech (Name_kind_type_tech, Type_technology_id) VALUES ('"+Name_kind_type_tech+"', '"+typeIdSelect+"')"
    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      res.send("Success")
    })
  } catch(err) {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }
})

api.post('/management/editKind/:id', async (req,res) => {
	let Kind_type_tech_Id = req.params.id
	let Name_kind_type_tech = req.body.Name_kind_type_tech

	try {

    var querySQL = "UPDATE kind_type_tech\
										SET Name_kind_type_tech='"+Name_kind_type_tech+"'\
										WHERE Kind_type_tech_Id='"+Kind_type_tech_Id+"' "
    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      res.send("Success")
    })
  } catch(err) {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }

})

module.exports = api
