const api = require('express').Router()
const db = require('../../model/connect_DB')
const sha512 = require('js-sha512')

api.use(require('../../model/session'))// session

api.post('/management/newUser/', async (req,res) => {
	let First_name = req.body.First_name
	let Last_name = req.body.Last_name
	let Tel = req.body.Tel
	let Email = req.body.Email
	let Username = req.body.Username
	let Password = req.body.Password
	let Permission = req.body.Permission
	Password = sha512(sha512(Password))

	try {

    var querySQL = "INSERT INTO user (First_name,\
                                      Last_name,\
                                      Tel,\
                                      Email,\
																			Username,\
																			Password,\
																			Permission_id)\
                                      VALUES ('"+First_name+"',\
                                              '"+Last_name+"',\
                                              '"+Tel+"',\
																							'"+Email+"',\
																							'"+Username+"',\
																							'"+Password+"',\
                                              '"+Permission+"')"
    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      res.send("Success")
    })
  } catch(err) {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }
})

api.post('/management/editUser/:id', async (req,res) => {
	let User_Id = req.params.id
	let First_name = req.body.First_name
	let Last_name = req.body.Last_name
	let Tel = req.body.Tel
	let Email = req.body.Email
	let Username = req.body.Username
	let Password = req.body.Password
	let Permission = req.body.Permission

	try {

    var querySQL = "UPDATE user\
										SET First_name='"+First_name+"',\
												Last_name='"+Last_name+"',\
												Tel='"+Tel+"',\
												Email='"+Email+"',\
												Username='"+Username+"',\
												Password='"+Password+"',\
												Permission_id='"+Permission+"'\
										WHERE User_Id='"+User_Id+"' "
    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      res.send("Success")
    })
  } catch(err) {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }

})

module.exports = api
