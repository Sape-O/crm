const api = require('express').Router()
const db = require('../../model/connect_DB')

api.use(require('../../model/session'))// session

// query for list all project of customer
api.get('/project/:id', async (req, res) => {
  let Customer_id = req.params.id
  try {
    let querySQL = "SELECT * FROM project WHERE project.Customer_id = '"+Customer_id+"' "

    /*let querySQL = " SELECT * FROM project \
                      INNER JOIN project_type_tech ON project.Project_Id = project_type_tech.Project_id\
                      INNER JOIN customers ON project.Customer_id = customers.Customer_Id\
                      INNER JOIN vendor ON vendor.Vendor_Id = project_type_tech.Vendor_id\
                      INNER JOIN kind_type_tech ON project_type_tech.Kind_type_tech_id = kind_type_tech.Kind_type_tech_Id\
                      INNER JOIN type_technology ON kind_type_tech.Type_technology_id = type_technology.Type_technology_Id\
                      WHERE customers.Customer_Id = '"+Customer_id+"' "
                      */
    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      else {
        for(var i=0;i<results.length;i++) {
          results[i].Estimate = new Date(results[i].Estimate)
          results[i].Estimate =  ((results[i].Estimate.getDate()+1 <10?'0':'') + (results[i].Estimate.getDate() + 1))
                             + "-" + ((results[i].Estimate.getMonth()+1 <10?'0':'')+(results[i].Estimate.getMonth() + 1))
                             + "-" + results[i].Estimate.getFullYear()
        }
        res.send(results)
      }

    })
  } catch {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }

})

// get for show add_edit.js
api.get('/project/edit/:id', async (req, res) => {

  try {
    let querySQL = " SELECT * FROM project_type_tech\
					            INNER JOIN project ON project_type_tech.Project_id = project.Project_Id\
                      INNER JOIN customers ON customers.Customer_Id = project.Customer_id\
					            INNER JOIN vendor ON vendor.Vendor_Id = project_type_tech.Vendor_id\
                      INNER JOIN kind_type_tech ON project_type_tech.Kind_type_tech_id = kind_type_tech.Kind_type_tech_Id\
                      INNER JOIN type_technology ON kind_type_tech.Type_technology_id = type_technology.Type_technology_Id\
                      WHERE project.Project_Id = '"+req.params.id+"' "

    db.query(querySQL,(err,results) => {
      if(err) res.send("0")
      else {
        for(var i=0;i<results.length;i++) {
          results[i].Estimate = new Date(results[i].Estimate)
          results[i].Estimate = ((results[i].Estimate.getMonth()+1 <10?'0':'')+(results[i].Estimate.getMonth() + 1))
                             + "/" + ((results[i].Estimate.getDate()+1 <10?'0':'') + (results[i].Estimate.getDate() + 1))
                             + "/" + results[i].Estimate.getFullYear()
          results[i].Estimate = new Date(results[i].Estimate)
        }
        res.send(results)
      }

    })
  } catch {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }

})

api.post('/project/newproject/:id', async (req, res) => {

  let Customer_id = req.params.id
  let Amount = req.body.amount
  let DealStatus = req.body.dealStatus
  let Estimate = new Date(req.body.estimate).toISOString().slice(0, 19).replace('T', ' ')
  let NameProject = req.body.nameProject
  let Technology = req.body.technology

  try {
    let querySQL = "INSERT INTO project (Customer_id, Amount, Estimate, Deal_status, NameProject)\
                    VALUES ('"+Customer_id+"', '"+Amount+"', '"+Estimate+"','"+DealStatus+"', '"+NameProject+"') "

    let queryDB = (callback) => {
      db.query(querySQL,(err,results) => {
        if(err) res.send("0")
        else callback(results)
      })

    }
    queryDB((callback) => {
      try{

        let querySQL2 = "INSERT INTO project_type_tech (Project_id, Kind_type_tech_id, Vendor_id) VALUES"
        let project_id = callback.insertId
        for (var i=0; i<Technology.length;i++) {
          if(i==Technology.length-1){
            querySQL2 = querySQL2 + "("+project_id+","+Technology[i].kind+","+Technology[i].vendor+")"
          }else{
            querySQL2 = querySQL2 + "("+project_id+","+Technology[i].kind+","+Technology[i].vendor+"),"
          }
        }
        db.query(querySQL2,(err,results2) => {
          if(err) res.send("0")
          res.send("Success")
        })
      } catch {
        res.send("false")
      }
    })

  } catch {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }

})

api.post('/project/edit/:id', async (req, res) => {
  // id = project id
  let Project_id = req.params.id
  let NameProject = req.body.nameProject
  let Amount = Number(req.body.amount)
  let DealStatus = req.body.dealStatus
  let Estimate = new Date(req.body.estimate).toISOString().slice(0, 19).replace('T', ' ')
  let Technology = req.body.technology
  console.log(Amount)
  //let Project_id_Send = req.body.project_id_Send  // not use chenge to delete all and insert new all
  //  Project_id_Send == Technology.length
  var error=0
  try {

    //------------ update in project only -------------//

    let querySQL = "UPDATE project\
                    SET Amount='"+Amount+"',\
                        Estimate='"+Estimate+"',\
                        Deal_status='"+DealStatus+"',\
                        NameProject='"+NameProject+"'\
                    WHERE Project_Id='"+Project_id+"' "

    db.query(querySQL,(err,results) => {
      if(err){error = error + 1}
    })

    let queryDB = "DELETE FROM project_type_tech WHERE Project_id='"+Project_id+"'"
    var callQuery = (callback) => {
      db.query(queryDB,(err,results) => {
        if(err) {error = error + 1}
        else {
          callback(true)
        }
      })
    }

    callQuery((callback) => {

      if(callback === true){

        try{

          let querySQL3 = "INSERT INTO project_type_tech (Project_id, Kind_type_tech_id, Vendor_id) VALUES"
            for (var i=0; i<Technology.length;i++) {
            if(i==Technology.length-1){
              querySQL3 = querySQL3 + "("+Project_id+","+Technology[i].kind+","+Technology[i].vendor+")"
            }else{
              querySQL3 = querySQL3 + "("+Project_id+","+Technology[i].kind+","+Technology[i].vendor+"),"
            }
          }

      //  let querySQL3 = "INSERT INTO project_type_tech (Project_id, Kind_type_tech_id, Vendor_id) VALUES (47,3,4),(47,2,4),(47,3,4),(47,2,4),(47,1,4)"
          db.query(querySQL3,(err,results2) => {
            if(err) {error = error + 1}
          })
        } catch {
          res.send("false")
        }

        if(error == 0){
          res.send("Success")
        } else {
          res.send("error for update")
        }
      }else {
        res.send("Success")
      }
    })


  } catch {
    res.status(400).send(err)
    let dateError = new date()
    console.log("date error : "+dateError + err)
  }

})

/*
api.delete('/project/delete/:id', async (req, res)=> { // Dangerus for only supper admin

  try {
    res.send("Delete : /project/delete/"+req.params.id)
  } catch {
    res.status(400).send(err)
  }

})
*/
module.exports = api
