const express = require('express')
const bodyParser = require('body-parser')
const db = require('./model/connect_DB')
var cors = require('cors')

const app = express()
const PORT = 3200

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(require('./api'))

db.connect(function(err) {
  if (err) throw err
  console.log("Connected!")
})

app.listen(PORT,()=>{
  console.log(`App CRM NODE_JS is running at port ${PORT}`)
})
